package com.technoloid.itfestevents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int numberOfClikcs = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Button myButton = (Button) findViewById(R.id.button_start);

//        myButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // do whatever here
//                Log.d("ITFEST", "Button clicked");
//            }
//        });
    }

    public void buttonClicked(View view) {
        Log.d("ITFEST", "Button clicked function");
        Intent intent = new Intent(this, DetailActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        numberOfClikcs ++;
        if (numberOfClikcs == 1) {
            Toast.makeText(this, "Press back again to exit!", Toast.LENGTH_LONG).show();
        } else {
        }
    }
}
