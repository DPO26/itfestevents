package com.technoloid.itfestevents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Log.d("Detail", "onCreate()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Detail", "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Detail", "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Detail", "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Detail", "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Detail", "onDestroy()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Detail", "onRestart()");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("Detail", "onBackPressed()");
        Toast.makeText(this, "Back pressed", Toast.LENGTH_LONG).show();
    }
}
